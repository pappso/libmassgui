libmassgui
==========

*libmassgui* is a C++ static library that is referenced from other projects.

The *libmassgui* static library is designed to enshrine the gui functionalities
needed by the following two projects:

* msXpertSuite/massXpert2;
* msXpertSuite/mineXpert2.

*libmassgui* has a companion GUI library, *libmass* , that is designed to
enshrine the non-GUI functionalities needed by the same two projects above.

