/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

/////////////////////// Std lib includes
#include <set>


/////////////////////// Qt includes
#include <QApplication>
#include <QDrag>
#include <QDebug>
#include <QMouseEvent>
#include <QMessageBox>
#include <QHeaderView>


/////////////////////// libmassincludes
#include <libmass/globals.hpp>
#include <libmass/Isotope.hpp>

/////////////////////// Local includes
#include "IsotopicDataTableView.hpp"


namespace msxps
{
namespace libmassgui
{


IsotopicDataTableView::IsotopicDataTableView(QWidget *parent_p)
  : QTableView(parent_p), mp_parent(parent_p)
{

  setAlternatingRowColors(true);

  setTextElideMode(Qt::ElideMiddle);
  setSortingEnabled(false);

  setSelectionMode(QAbstractItemView::ContiguousSelection);

  // Select items, not only rows or columns
  setSelectionBehavior(QAbstractItemView::SelectItems);

  QHeaderView *headerView = horizontalHeader();
  headerView->setSectionsClickable(true);
  headerView->setSectionsMovable(true);
}


IsotopicDataTableView::~IsotopicDataTableView()
{
}


void
IsotopicDataTableView::setIsotopicData(libmass::IsotopicDataSPtr isotopic_data_sp)
{
  msp_isotopicData = isotopic_data_sp;
}


libmass::IsotopicDataSPtr
IsotopicDataTableView::getIsotopicData()
{
  return msp_isotopicData;
}


QWidget *
IsotopicDataTableView::parent()
{
  return mp_parent;
}


void
IsotopicDataTableView::setParent(QWidget *parent_p)
{
  Q_ASSERT(parent_p);
  mp_parent = parent_p;
}


void
IsotopicDataTableView::selectionChanged(const QItemSelection &selected,
                                   const QItemSelection &deselected)
{
  // The selected selection parameter lists item that have undergone selection
  // during a (de)selection operation, not the items that have been effectively
  // committed to selection (release of key or release of mouse drag).
  emit selectionChangedSignal(selected, deselected);

  // Take advantage to emit a signal with a list of effectively committed
  // selected indices.
  emit allSelectedIndicesSignal(selectedIndexes());

  // Finally, take advantage to actually determine which unique rows do harbor
  // the selected indices (selection indices might contain multiple cells from a
  // given single row!)

  QModelIndexList selected_indices = selectedIndexes();

  std::set<int> rows_set;

  for(int iter = 0; iter < selected_indices.size(); ++iter)
    {
      rows_set.insert(selected_indices.at(iter).row());
    }

  emit selectedRowsChangedSignal(rows_set);
}


QModelIndexList
IsotopicDataTableView::selectionIndexes()
{
  return selectedIndexes();
}


} // namespace libmassgui

} // namespace msxps
