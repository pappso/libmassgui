/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QPlainTextEdit>


/////////////////////// pappsomspp includes
#include <limits>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>
#include <pappsomspp/processing/combiners/mzintegrationparams.h>

#include <libmass/PeakCentroid.hpp>
#include <libmass/MassPeakShaperConfig.hpp>
#include <libmass/MassPeakShaper.hpp>
#include <libmass/IsotopicClusterGenerator.hpp>


/////////////////////// Local includes
#include "ui_IsotopicClusterShaperDlg.h"
#include "MassPeakShaperConfigWidget.hpp"


namespace msxps
{

namespace libmassgui
{


enum class TabWidgetPage
{
  INPUT_DATA = 0x000,
  LOG,
  RESULTS,
};


class ProgramWindow;

class IsotopicClusterShaperDlg : public QDialog
{
  Q_OBJECT

  public:
  IsotopicClusterShaperDlg(QWidget *program_window_p,
                           const QString &applicationName,
                           const QString &description);

  IsotopicClusterShaperDlg(
    QWidget *program_window_p,
    const QString &applicationName,
    const QString &description,
    pappso::TraceCstSPtr isotopic_cluster_sp,
    int normalizing_intensity = std::numeric_limits<double>::min());

  virtual ~IsotopicClusterShaperDlg();

  void setIsotopiCluster(pappso::TraceCstSPtr isotopic_cluster_sp);

  // The normalizing intensity usually is the greatest intensity in the cluster
  // centroids that is to be used for normalization of the peaks in the cluster.
  void setNormalizingIntensity(double new_max_intensity);

  void setMassSpectrumTitle(const QString &title);
  void setColorByteArray(const QByteArray &color_byte_array);


  public slots:
  void traceColorPushButtonClicked();


  signals:
  void displayMassSpectrumSignal(const QString &title,
                                 const QByteArray &color_byte_array,
                                 pappso::TraceCstSPtr trace);

  private:
  Ui::IsotopicClusterShaperDlg m_ui;
  MassPeakShaperConfigWidget *mp_massPeakShaperConfigWidget = nullptr;

  QWidget *mp_programWindow = nullptr;

  QString m_applicationName;
  QString m_description;
  QString m_fileName;

  pappso::MapTrace m_mapTrace;
  pappso::Trace m_finalTrace;

  // For the mass spectra that are synthesized and served.
  QByteArray m_colorByteArray;


  pappso::MzIntegrationParams m_mzIntegrationParams;

  // Of all the peak centroids' intensities, what is the m/z value of the most
  // intense?
  double m_referencePeakMz = 0.0;

  int m_normalizingIntensity = 1;

  // This is the starting material for the shaping. Each peak centroid in this
  // Trace is the apex of a peak to be shaped. There can be as many peak
  // centroids a desired, they will all be treated separately.
  pappso::Trace m_isotopicCluster;

  // Each (m/z,i) pair (the i is in fact a probability that can later be
  // converted to a relative intensity) in the text edit widget will be
  // converted into a PeakShaper instance. Each PeakShaper instance will craft
  // the m_pointCount DataPoints to create the trace corresponding to the
  // starting peak centroid.
  QList<libmass::MassPeakShaperSPtr> m_peakShapers;

  libmass::MassPeakShaperConfig m_config;
  
  std::shared_ptr<QTimer> msp_msgTimer = std::make_shared<QTimer>();
  
  void writeSettings(const QString &configSettingsFilePath);
  void readSettings(const QString &configSettingsFilePath);

  void closeEvent(QCloseEvent *event);

  void setupDialog();

  QString craftMassSpectrumName();
  std::size_t fillInThePeakShapers();

  void message(const QString &message, int timeout = 3000);

  private slots:
  void importFromText();

  bool checkParameters();

  // void inputDataSelectionChanged();

  void run();
  void outputFileName();
  void displayMassSpectrum();
  void copyMassSpectrumToClipboard();

  void normalizingGrouBoxToggled(bool checked);
  void normalizingIntensityValueChanged();
};

} // namespace libmassgui

} // namespace msxps

