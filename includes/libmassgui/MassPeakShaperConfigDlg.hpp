/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QPlainTextEdit>


/////////////////////// pappsomspp includes
#include <limits>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>
#include <pappsomspp/processing/combiners/mzintegrationparams.h>

#include "libmass/PeakCentroid.hpp"
#include "libmass/MassPeakShaperConfig.hpp"
#include "libmass/MassPeakShaper.hpp"


/////////////////////// Local includes
#include "ui_MassPeakShaperConfigDlg.h"
#include "MassPeakShaperConfigWidget.hpp"


namespace msxps
{

namespace libmassgui
{


class MassPeakShaperConfigDlg : public QDialog
{
  Q_OBJECT

  public:
  MassPeakShaperConfigDlg(
    QWidget *program_window_p,
    const QString &applicationName,
    const QString &description);

  virtual ~MassPeakShaperConfigDlg();

  void writeSettings(const QString &configSettingsFilePath);
  void readSettings(const QString &configSettingsFilePath);

  // The normalizing intensity usually is the greatest intensity in the cluster
  // centroids that is to be used for normalization of the peaks in the cluster.
  void setNormalizingIntensity(double new_max_intensity);

  private:
  Ui::MassPeakShaperConfigDlg m_ui;
  MassPeakShaperConfigWidget *mp_massPeakShaperConfigWidget = nullptr;

  QWidget *mp_parent = nullptr;

  QString m_applicationName;
  QString m_fileName;

  libmass::MassPeakShaperConfig m_config;

  void closeEvent(QCloseEvent *event);

  void setupDialog();

signals:

  void updatedMassPeakShaperConfigSignal(const libmass::MassPeakShaperConfig &config);

  private slots:
};

} // namespace libmassgui

} // namespace msxps

