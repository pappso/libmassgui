/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// Std lib includes
#include <set>


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include <libmass/Isotope.hpp>
#include <libmass/IsotopicData.hpp>


namespace msxps
{

namespace libmassgui
{


class IsotopicClusterGeneratorDlg;


class IsotopicDataTableView : public QTableView
{
  Q_OBJECT

  private:
  QWidget *mp_parent;

  libmass::IsotopicDataSPtr msp_isotopicData;

  // For drag operations.
  QPoint m_dragStartPos;

  // We do not want to hide the parent class function here.
  using QAbstractItemView::startDrag;
  void startDrag();

  signals:
  void selectionChangedSignal(const QItemSelection &selected,
                              const QItemSelection &deselected);

  void allSelectedIndicesSignal(QModelIndexList selected_indices);

  void selectedRowsChangedSignal(std::set<int> selected_rows);

  public:
  IsotopicDataTableView(QWidget *parent = 0);
  ~IsotopicDataTableView();

  void setIsotopicData(libmass::IsotopicDataSPtr isotopic_data_sp);
  libmass::IsotopicDataSPtr getIsotopicData();

  QWidget *parent();
  void setParent(QWidget *parent_p);

  void selectionChanged(const QItemSelection &selected,
                        const QItemSelection &deselected);

  QModelIndexList selectionIndexes();
};

} // namespace libmassgui

} // namespace msxps

