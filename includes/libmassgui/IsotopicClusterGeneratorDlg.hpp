/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes
#include <QSortFilterProxyModel>
#include <QFormLayout>


////////////////////// pappso includes
#include <pappsomspp/trace/trace.h>


/////////////////////// libmass includes
#include <libmass/globals.hpp>
#include <libmass/Formula.hpp>
#include <libmass/PeakCentroid.hpp>
#include <libmass/Isotope.hpp>
#include <libmass/IsotopicData.hpp>
#include <libmass/IsotopicDataLibraryHandler.hpp>
#include <libmass/IsotopicDataUserConfigHandler.hpp>
#include <libmass/IsotopicDataManualConfigHandler.hpp>
#include <libmass/IsotopicClusterGenerator.hpp>


/////////////////////// Local includes
#include "IsotopicDataTableViewModel.hpp"
#include "ui_IsotopicClusterGeneratorDlg.h"


namespace msxps
{
namespace libmassgui
{


class IsotopicDataTableViewModel;
class IsotopicDataTableViewSortProxyModel;

using ArrayOfInt            = int *;
using ArrayOfDouble         = double *;
using ArrayOfArraysOfDouble = double **;

class IsotopicClusterGeneratorDlg : public QDialog
{
  Q_OBJECT

  public:
  IsotopicClusterGeneratorDlg(QWidget *program_window_p,
                              const QString &applicationName,
                              const QString &description);

  virtual ~IsotopicClusterGeneratorDlg();

  bool initializeIsoSpecManualConfigurationWidgets(
    const libmass::IsotopicDataManualConfigHandler &config_handler);

  public slots:

  // std::pair<QLineEdit *, QSpinBox *> addElementSkeletonGroupBox();
  QGroupBox *addElementSkeletonGroupBox();
  QGroupBox *addElementGroupBox();
  void removeElementGroupBox();

  QFrame *createIsotopeFrame(QGroupBox *elementGroupBox = nullptr);
  std::pair<QDoubleSpinBox *, QDoubleSpinBox *> addIsotopeFrame();
  void removeIsotopeFrame();

  /////////////////////// The run functions //////////////////////
  /// These function are adapted to the three different contexts

  // Running IsoSpec computations based on the static library isotopic data
  bool runLibrary();
  // Running IsoSpec computations based on the user config isotopic data
  bool runUserConfig();
  // Running IsoSpec computations based on the manual config isotopic data
  bool runUserManualConfig();

  void toIsotopicClusterShaper();

  void addLibraryFormula();
  void removeLibraryFormula();

  void addUserConfigFormula();
  void removeUserConfigFormula();

  void traceColorPushButtonClicked();


  signals:
  void displayMassSpectrumSignal(const QString &title,
                                 const QByteArray &color_byte_array,
                                 pappso::TraceCstSPtr trace);

  private:
  Ui::IsotopicClusterGeneratorDlg m_ui;

  QString m_applicationName;
  QString m_fileName;

  libmass::Formula m_formula;

  void closeEvent(QCloseEvent *event);

  QWidget *mp_programWindow = nullptr;

  int m_normalizeIntensity      = std::numeric_limits<int>::min();
  double m_maxSummedProbability = 0.95;
  int m_charge                  = 1;

  pappso::SortType m_sortType = pappso::SortType::no_sort;

  pappso::SortOrder m_sortOrder = pappso::SortOrder::ascending;

  ///////////// Isotopic data for three different contexts ////////////

  // Isotopic data from the IsoSpec library tables
  libmass::IsotopicDataSPtr msp_isotopicDataLibrary = nullptr;
  // Isotopic data from the user's personal file
  libmass::IsotopicDataSPtr msp_isotopicDataUserConfig = nullptr;
  // Isotopic data from the user's personal GUI-based manual config file
  libmass::IsotopicDataSPtr msp_isotopicDataUserManualConfig = nullptr;

  ///////////// Table view models for two different contexts ////////////
  // The table view model that manages the library static IsoSpec standard data
  IsotopicDataTableViewModel *mpa_libraryStaticTableViewModel;

  // The table view model that manages the user config IsoSpec standard data
  IsotopicDataTableViewModel *mpa_userStaticTableViewModel;

  // For the mass spectra that are synthesized and served.
  QByteArray m_colorByteArray;

  // Note that there is no model for the user manual config data because this
  // configuration is not entered in a table view but using another paradigm.

  libmass::IsotopicClusterGenerator m_clusterGenerator;

  void writeSettings(const QString &configSettingsFilePath);
  void readSettings(const QString &configSettingsFilePath);

  std::size_t validateManualConfig(
    libmass::IsotopicDataManualConfigHandler &config_handler);

  void setupIsoSpecStandardStaticTableView();
  void setupIsoSpecStandardUserTableView();
  bool setupDialog();

  bool loadLibrary();
  bool saveLibrary();

  bool loadUserConfig();
  bool saveUserConfig();

  bool loadUserManualConfig();
  bool saveUserManualConfig();

  bool checkParameters();

  void reportResults();

  void message(const QString &message, int timeout = 3000);
};

} // namespace libmassgui

} // namespace msxps

